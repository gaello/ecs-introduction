﻿using UnityEngine;
using Unity.Entities; // ECS
using Unity.Collections; // Native Collections for ECS
using Unity.Transforms; // Transforms - position, rotation, scale for ECS
using Unity.Rendering; // Rendering for ECS
using Unity.Mathematics; // Types for ECS.

/// <summary>
/// This class initialize ECS on the scene with some spinny cubes.
/// </summary>
public class GameManager : MonoBehaviour
{
    // Reference to the Entity Manager, needed for ECS.
    private EntityManager entityManager;

    [Header("Entities Config")]
    // Number of entities to spawn.
    [SerializeField]
    private int numberOfEntitiesToSpawn = 10;

    // Config for entities
    [SerializeField]
    private EntitiesConfig config = new EntitiesConfig();

    /// <summary>
    /// Unity method called on first frame.
    /// </summary>
    private void Start()
    {
        Initialize();
    }

    /// <summary>
    /// Initialize scene with spinning cubes.
    /// </summary>
    private void Initialize()
    {
        // Assigning reference to EntityManager.
        entityManager = World.Active.EntityManager;

        // Creating an empty array for cube entities.
        NativeArray<Entity> cubeEntities = new NativeArray<Entity>(numberOfEntitiesToSpawn, Allocator.Temp);

        // Creating archetype for cube. It's like a prefab.
        var cubeArchetype = entityManager.CreateArchetype(
                typeof(RenderMesh), // Rendering mesh
                typeof(LocalToWorld), // Needed for rendering
                typeof(Translation), // Transform position
                typeof(Rotation), // Transform rotation
                typeof(NonUniformScale), // Transform scale (version with X, Y and Z)
                typeof(Rotator) // Our custom component
            );


        // Creating entities of provided archetype to fill the list.
        entityManager.CreateEntity(cubeArchetype, cubeEntities);

        // Creating Random Number Generator.
        var rnd = new Unity.Mathematics.Random();
        rnd.InitState((uint)System.DateTime.UtcNow.Ticks);

        // Placing cubes around the scene.
        for (int i = 0; i < cubeEntities.Length; i++)
        {
            var cubeEntity = cubeEntities[i];

            // Setting rendering.
            entityManager.SetSharedComponentData(cubeEntity, new RenderMesh { mesh = config.Mesh, material = config.Material });

            // Setting position.
            entityManager.SetComponentData(cubeEntity, new Translation { Value = rnd.NextFloat3(new float3(config.MinEntityPosition.x, 0, config.MinEntityPosition.y), new float3(config.MaxEntityPosition.x, 0, config.MaxEntityPosition.y)) });
            // Setting scale.
            entityManager.SetComponentData(cubeEntity, new NonUniformScale { Value = rnd.NextFloat3(new float3(config.MinEntityScale.x, config.MinEntityScale.y, config.MinEntityScale.x), new float3(config.MaxEntityScale.x, config.MaxEntityScale.y, config.MaxEntityScale.x)) });

            // Setting our rotator component.
            entityManager.SetComponentData(cubeEntity, new Rotator { Rotation = rnd.NextFloat(0, 360), RotationSpeed = rnd.NextFloat(config.MinSpin, config.MaxSpin) });
        }

        // We have to dispose native collections ourselves!
        cubeEntities.Dispose();
    }
}
