﻿using UnityEngine;

/// <summary>
/// This class contains config for the Entities in this example.
/// </summary>
[System.Serializable]
public class EntitiesConfig
{
    [Header("Rendering")]
    [SerializeField]
    private Mesh mesh;
    public Mesh Mesh => mesh;

    [SerializeField]
    private Material material;
    public Material Material => material;

    [Header("Position")]
    [SerializeField]
    private Vector2 minEntityPosition;
    public Vector2 MinEntityPosition => minEntityPosition;

    [SerializeField]
    private Vector2 maxEntityPosition;
    public Vector2 MaxEntityPosition => maxEntityPosition;

    [Header("Spin")]
    [SerializeField]
    private float minSpin;
    public float MinSpin => minSpin;

    [SerializeField]
    private float maxSpin;
    public float MaxSpin => maxSpin;

    [Header("Scale")]
    [SerializeField]
    private Vector2 minEntityScale;
    public Vector2 MinEntityScale => minEntityScale;

    [SerializeField]
    private Vector2 maxEntityScale;
    public Vector2 MaxEntityScale => maxEntityScale;
}
