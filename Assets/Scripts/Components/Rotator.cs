﻿using Unity.Entities;

/// <summary>
/// Rotator component.
/// Stores rotation data.
/// </summary>
public struct Rotator : IComponentData
{
    public float Rotation;
    public float RotationSpeed;
}
