﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

/// <summary>
/// Rotator system.
/// This system grabs all Rotators and rotate them.
/// </summary>
public class RotatorSystem : ComponentSystem
{
    /// <summary>
    /// System Update, works like regular Update in MonoBehaviours.
    /// </summary>
    protected override void OnUpdate()
    {
        // Grabs all entities with Rotator and Rotation components attached to them.
        Entities.WithAll<Rotator>().ForEach((Entity entity, ref Rotator rotator, ref Rotation rotation) =>
        {
            // Updating rotation.
            rotator.Rotation += rotator.RotationSpeed * Time.deltaTime;

            // Assigning new rotation in Rotation Component (equivalent to Transform.rotation)
            rotation.Value = quaternion.RotateY(rotator.Rotation);
        });
    }
}
